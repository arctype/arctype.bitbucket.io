/**
 *
 * app.js
 */


 let theVideo;
 let isVideo = true;
 let isKeyPoints = true;
 let poseNet;
 let poses = [];
 let skeletons = [];
 let var1; //font weight
 let var2; // font width
 let var3; // font size
 let var4; // inverted
 let var5; // fontWidthHands

 // features
 let mNet;
 let regress;
 let value;
 let isLoad = true;


 function setup() {
  let myCanvas = createCanvas(640, 480);
  //myCanvas.parent('theCanvas');
  background(0);
  theVideo = createCapture(VIDEO);
  theVideo.size(width, height);

  // Create a new poseNet method with a single detection
  poseNet = ml5.poseNet(theVideo, modelReady);
  // This sets up an event that fills the global variable "poses"
  // with an array every time new poses are detected
  poseNet.on('pose', function(results) {
    poses = results;
  });
  // Hide the video element, and just show the canvas
  theVideo.hide();

  // Feature learning :
  mNet = ml5.featureExtractor('MobileNet', theVideo, modelReady);
  regress = mNet.regression(theVideo, videoReady);

  //regress.load(); regress.predict(gotResults);
  var theTestButton = document.getElementById('testButton');
  theTestButton.addEventListener("click", function(ev){
      //regress.load();
      regress.predict(gotResults);
      //isClassify = true;

});

}


function draw() {
  background(255);
  if (isVideo) {
    image(theVideo, 0, 0);
  }else {
    /*const cnv = document.getElementById('theCanvas');
    cnv.style.visibility="hidden";  */ // ???? WORKS ???
}

  if (poses.length > 0) {
    trackKeypoints();
  }


    var2 = value * 800; // Q&D
    //console.log(fontW);
    const txtElt = document.getElementById('textSlide1');
    txtElt.style.setProperty('--font-weight', var1);

    //const txtElt2 = document.getElementById('textSlide2');
    //txtElt2.style.setProperty('--font-weight', var2);
      //txtElt.style.fontSize = fSize;

    const txtElt3 = document.getElementById('inverted');
    txtElt3.style.setProperty('--font-weight', var4);

    const txtElt4 = document.getElementById('inverted2');
    txtElt4.style.setProperty('--font-weight', var4);

    const txtElt5 = document.getElementById('inverted3');
    txtElt5.style.setProperty('--font-weight', var4);

    const txtElt6 = document.getElementById('textSlide2');
    txtElt6.style.setProperty('--font-width', var5);



}




// A function to track & draw detected keypoints
function trackKeypoints() {
  // Loop through all the poses detected
  for (let i = 0; i < poses.length; i++) {
    // For each pose detected, loop through all the keypoints
    for (let j = 0; j < poses[i].pose.keypoints.length; j++) {
      // A keypoint is an object describing a body part (like rightArm or leftShoulder)
      let keypoint = poses[i].pose.keypoints[j];

      let keypoint1 = poses[0].pose.keypoints[1];
      let keypoint2 = poses[0].pose.keypoints[2];
      let keypoint3 = poses[0].pose.keypoints[9];
      let keypoint4 = poses[0].pose.keypoints[10];


      // Only draw an ellipse if the pose probability is bigger than 0.2
      if (keypoint.score > 0.2) {
        // maybe multiply by a factor or normalize this?
        let d = dist(keypoint1.position.x, keypoint1.position.y, keypoint2.position.x, keypoint2.position.y);
        //console.log('d = '+d);
        var1 = map(d, 10, 80, 1000, 0); // Important to avoid negative values
        //var1 = lerp(var1, d, 0.1); // perhaps I need to implement this in css : animation
        var4 = map(d, 20, 90, 0, 1000 );
        //var4 = lerp(var4, d, 0.1);

        let d2 = dist(keypoint3.position.x, keypoint3.position.y, keypoint4.position.x, keypoint4.position.y);
        //console.log(d2);
        var5 = map(d2, 80, 550, 0, 1000 );

        if(isKeyPoints){
          fill(255);
          textSize(18);
          text("keypoint" + j, keypoint.position.x, keypoint.position.y);
          fill(255, 0, 0);
          noStroke();
          ellipse(keypoint.position.x, keypoint.position.y, 10, 10);


          fill(255, 200, 0);
          ellipse(keypoint1.position.x, keypoint1.position.y, d / 2, d / 2);
          ellipse(keypoint2.position.x, keypoint2.position.y, d / 2, d / 2);
        }
      }
    }
  }

}

function gotResults(err, result) {
  if (err) {
    console.error(err);
  } else {
    value = result;
    regress.predict(gotResults);
    isText = true;

  }
}

function videoReady() {
  console.log("Video is ready");
  if(isLoad){
      regress.load('models/model.json', function(){
        console.log("model loaded");
    });
  }
}

function modelReady() {
  console.log("video loaded");
}
